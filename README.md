# Learning steps #

Generally speaking, there are two main enemies when trying to learn any thing:

- ***Seeking quick and dirty shortcuts***:  There are no shortcuts in becoming great at anything. But most people usually seek shortcuts - something they will never find.
- ***Illusion of knowing***: This is extremely dangerous and usually, its existence is not even acknowledged. I have a simple tool to diagnose this disease

Have a handle on those two and you are good to go.

Assuming you are sincerely interested in learning, **what are the steps you might follow to learn a new technology?**

How to learn this fast? is perhaps among the most frequently asked questions. Most folks asking this are looking to master it over night. They didn't say it outloud but that is mostly what they are asking - shortcut.

Learning anything is journey/a process, and learning technology is no different. If you are looking shortcut, I have bad news my friend. ***THERE IS NO SHORTCUT*** to master anything!

Hmmm, on a second thought, I realized there are two ways to optimize your time:

```
1) Having a mentor or
2) Being strategic on your resource choice, process selection & time management.
```

Other than that, there is no shortcut! So if you are looking one, don't waste yourtime. Just close this page and move on to that cat video on youtube. This isn't for you!

But if you are not, then this short article may give you some structured guidance on how to approach learning new technology.

## 1) Start with **Quick Start** documentation from official website ##

At this point, since you can't really know if the video on *youtube*, or other platforms is outdated, latest, released by someone who truly knows the subject, or is for someone new to the technology, you might spend your valuable time on a material not suited for you. This not only wastes your time but can send a wrong message that the technology may seem too difficult to learn - leading some to quite early.

For that reason, the official website is the best place to start exploring.
```
Before you start reading a book or watch videos, you should visit the official website and look for Quick Start.

Follow their instruction & PRACTICE ALL examples shown. NO COPY PASTE!
```
Now, if you need the technology for really something quick and have no plan for use it again, you might apply the skill you gained and move on with life :)

But chances are, you will be using it for quite sometime. In which case, go a little bit deeper.

## 2. Watch step by step video tutorials from known source ##

```

At this point, you have basic understanding from step #1 and should go deeper but not too deep.

DO ALL examples on your own. DO NOT COPY PASTE! I know, I know, you think you know that line of code and want to save time, but save time for what? Coding isn't a knowledge, its a skill and hence require practice! Type the examples yourself, it won't kill you :)

```
Again, if you are not going to use the technology for long, you might stop here. But chances are, you will be using it for longer.

## 3. Read books & specialized blog posts ##

```

At this point, you have good understand and you can be very selective in the book & blog posts selection and go deeper.

Here, you should not stop on how to use something but start learning the internals of how the technology works.

Like in the above 2 steps, PRACTICE PRACTICE PRACTICE.... There is nothing like LEARNING BY DOING

```

Clearly, if you are on this step, you have a plan to use the technology for many years to come. Next...

## 4. Build something with it on your own ##

```

Now you know the ins & outs of the technology, you can cement it even more by applying the various pieces learnt so far to build something of your own.

The app you build don't have to be released for the general public. But you might also make something of value for millions of users and get rich along the way. Who knows :)

At this point, you should also start blogging - sharing or not sharing it with the general public is upto you.

Be a mentor. DO NOT SOLVE their problem - instead, guide them so they can learn & solve on their own.

```

***ILLUSION of KNOWING***

Often times, you may think you know something and hence may not take the necessary action to fill the gap needed. After all, you know it all, right? To make matters worse, there wasn't any simple strategy to guage yourself if you do understand something or whether it was just an illusion. Until now...

I have a very simple strategy to test your understanding.
Ask yourself this:

***CAN I EXPLAIN THE MATTER TO ANYONE WHO HAS NO PRIOR KNOWLEDGE IN VERY SIMPLE TERMS SO THEY CAN UNDERSTAND IT?***

```

If the answer is YES: GREAT - you truly understood it!

If the answer is NO however, I am afraid, I have bad news my friend. What you have is an ILLUSION of knowing - extremly dangerious. Be honest to yourself & fill the gap by spending time to learn what is missing.

```

In a way, the shortcut to learning & being great at anything is to be realstic, strategic and practical.

***Realstic***

- Never attempt to master anything over night. We all forgot but we never learn how to eat in one shot, nor how to walk or pretty much anything.
- Ok, childhood memory is perhaps too far to remember but how about driving? Did you jump into the car & become so efficient in one go & drove on the highway? I will leave the answer to yourself.
- Hammering yourself with unrealstic expectation has no use other than to feel bad/a failure.
- When this frustration is so high, most likely than not, it force you to quit. It won't stop there too. It misleads you in belevieng that you are actually not capable of learning something new. This is extremely dangerious. That is why I hear people saying: Do you think I can do this? Imagine that! Any human being(unless they have brain damange) can learn anything. But unrealstic expectations lead to such degrading mindset that the person might stop even trying anything. For those who believe they can't learn, I have few questions:

```

  1) How do you know that you can't?

  2) Have you tried it?

  3) Did you go through what's required?

  4) Were you committed - giving it all you got or were you just physically there?

  5) Why do you think others were able to do it? God made them special but He made you intentionally stupid? Or did they work a bit harder and strtegically to get better at it?

  6) Did you know anyone born with:
      - coding skill?
      - running skill?
      - swimming skill?
      - dancing skill or
      - really anything?
      Or did they aquired that with practice? Why then you are such special to believe you can't do the same?

```

- Hopefully, you got the picture and that is why you should be realstic in your expectations!

***Strategic***

  - This is very very critical. The optimal strategy is to find the best resource and chunck it up to develop the required skill gradually.
  - Failure to do this means, you will be jumping between irrelevant materials here and there but won't progress as far as your effort.
  - Being strategic means finding the optimal means to progress faster
  - Have a mentor - they will show you the way

***Practical***

- You should commit yourself and do everything necessary. Especially, at the begning, it may seem like you aren't going as fast. Remember, building the foundation is slow but after that, it will accelerate like nothing you never seen before.
- If you want to compete walking, you excercise walking, if you want to compete for 5k, you prepare for 5k, initially by regular walking few blocks, and then jogging with a bit faster pace, and then running slowly, and then faster and faster until you are ready for a win. For 10k, and marathon, different routine but over all strategy remain the same - gradually getting better with time.
- Without doing whats necessary, you should never then expect the outcome. Acquiring skill of any kind including coding is no different.
- You have to do what needs to be done!


Interested to understand the what, why and how to mitigate [Procrastination?](https://bitbucket.org/ZelalemW/pomodoro/src/master/)


***Happy Learning!***
